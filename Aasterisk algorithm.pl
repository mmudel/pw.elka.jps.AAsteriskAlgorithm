/* Przyklad wywo�ania */
/* Zdefiniowany model dla gry �semka (prostsza wersja pi�tnastki) */
start_A_star([pos(0,3/1), pos(1,1/1), pos(2, 2/1), pos(3, 1/2), pos(4, 2/2), pos(5,3/2), pos(6, 1/3), pos(7, 2/3), pos(8,3/3)], Wynik).
/*##############################################*/

hScore([pos(N, X/Y) | R], Score) :-
	hScore(R, Score1),
	hScore(pos(N, X/Y), Score2),
	Score is Score1 + Score2.
	
hScore([], 0).

hScore(pos(0, _/_), 0) :- !.

hScore(pos(N, X/Y), Score) :-
	XDest is N mod 3 + 1,
	YDest is N div 3 + 1,
	Score is abs(X - XDest) + abs(Y - YDest).

succ([pos(0,EmptyPos)|TilePositions], ->(EmptyPos, NewEmptyPos), 1, [pos(0, NewEmptyPos)|NewTilePositions]):-
	find_neighbour(EmptyPos, TilePositions,NewEmptyPos, NewTilePositions).

find_neighbour(EmptyPos, [pos(Neighb, NeighbPos)|RestPositions],
	NeighbPos, [pos(Neighb, EmptyPos)|RestPositions]):-
	adjacent(EmptyPos, NeighbPos).
	
find_neighbour(EmptyPos, [T|RestPositions], NewEmptyPos, [T|NewPositions]):-
		find_neighbour(EmptyPos, RestPositions, NewEmptyPos, NewPositions).

adjacent(X1/Y, X2/Y):-
	DiffX is X1-X2,
	abs(DiffX,1).
	
adjacent(X/Y1, X/Y2):-
	DiffY is Y1-Y2,
	abs(DiffY,1).
	
abs(X,X):-
		X>=0,!.

abs(X, AbsX) :-
	AbsX is -X.		
			
goal(node(State, _,_, _, _)) :-
	hScore(State, 0).

start_A_star(InitState, PathCost):-
  score(InitState, 0, 0, InitCost, InitScore) ,
  search_A_star([node(InitState, nil, nil, InitCost , InitScore)], [], PathCost, 3, 3).

search_A_star(OpenQueue, ClosedSet, PathCost, N, Limit) :-
	fetch(Node, OpenQueue, ClosedSet, RestQueue, N),
	continue(Node, RestQueue, ClosedSet, PathCost, N, Limit),
    !.
	
search_A_star(Queue, ClosedSet, PathCost, IleW, Limit) :-
    write("Yes/No"),
    read("yes"),
    NewLimit is Limit + 5,
	search_A_star(Queue, ClosedSet, PathCost, IleW, NewLimit).
	
continue(node(State, Action, Parent, Cost, _), _, ClosedSet,
         path_cost(Path, Cost), _, _) :-
    goal(node(State, Action, Parent, Cost, _)), !,
	build_path(node(Parent, _, _, _, _), ClosedSet, [Action/State], Path).

continue(Node, RestQueue, ClosedSet, Path, IleW, Limit) :-
	Limit > 0,
	NewLimit is Limit - 1,
	expand(Node, NewNodes),
	insert_new_nodes(NewNodes, RestQueue, NewQueue),
	search_A_star(NewQueue, [Node|ClosedSet], Path, IleW, NewLimit).

fetch(W, RestQueue, ClosedSet, NewRest, IleW) :-
    show(Result, RestQueue, ClosedSet, IleW),
    write(Result),
    read(ListaWyboru),	
    takeIndex(X, ListaWyboru, Reszta),
    fetchNode(W, RestQueue, X, ClosedSet, NewRest).

fetchNode(node(State, Action,Parent, Cost, Score),
      [node(State, Action,Parent, Cost, Score)|R], 1, ClosedSet, R) :-
    not(member(node(State, _,_, _, _), ClosedSet)),
    !.

fetchNode(X, [node(State, Action,Parent, Cost, Score)|R], Which, ClosedSet,
      [node(State, Action,Parent, Cost, Score)|R1]) :-
    not(member(node(State, _,_, _, _), ClosedSet)),
    Which1 is Which-1,
    fetchNode(X, R, Which1, ClosedSet, R1),
    !.

fetchNode(X, [_|R], Which, ClosedSet, R1) :-
    fetchNode(X, R, Which, ClosedSet, R1).

show([], [], _, _).

show([node(State, Action,Parent, Cost, Score)],
     [node(State, Action,Parent, Cost, Score) |RestQueue], ClosedSet, 1) :-
    not(member(node(State, _, _, _, _), ClosedSet)).
    
show([node(State, Action,Parent, Cost, Score) |Result],
	[node(State, Action,Parent, Cost, Score) |RestQueue], ClosedSet, IleW) :-
    not(member(node(State, _, _, _, _), ClosedSet)),
    IleW1 is IleW-1,
    show(Result, RestQueue, ClosedSet, IleW1),
    !.
    
show(A, [ _ |RestQueue], ClosedSet, IleW1) :-
    show(A, RestQueue, ClosedSet, IleW1).

takeIndex(X, [X|R], R).
takeIndex(X, [Y|R], [Y|R1]) :-
	takeIndex(X, R, R1).
		
expand(node(State, _ ,_ , Cost, _ ), NewNodes)  :-
	findall(node(ChildState, Action, State, NewCost, ChildScore),
            (succ(State, Action, StepCost, ChildState),
                score(ChildState, Cost, StepCost, NewCost, ChildScore)),
            NewNodes),
    !.
 
expand( _, [] ).
 
 
score(State, ParentCost, StepCost, Cost, FScore):-
	Cost is ParentCost + StepCost ,
	hScore(State, HScore),
	FScore is Cost+ HScore.

insert_new_nodes([], Queue, Queue).

insert_new_nodes( [Node|RestNodes], Queue,NewQueue ):-
	insert_p_queue(Node, Queue, Queue1),
	insert_new_nodes( RestNodes, Queue1, NewQueue).

insert_p_queue(node(State, Action, Parent, Cost, FScore), [],
               [node(State, Action, Parent, Cost, FScore)]).

insert_p_queue(node(State, Action, Parent, Cost, FScore),
[node(State1, Action1, Parent1, Cost1, FScore1)|RestQueue],
[node(State1, Action1, Parent1, Cost1, FScore1)|Rest1] ):-
	FScore >= FScore1,!,
	insert_p_queue(node(State, Action, Parent, Cost, FScore), RestQueue, Rest1).

insert_p_queue(node(State, Action, Parent, Cost, FScore), RestQueue,[node(State, Action, Parent, Cost, FScore)|RestQueue]).

build_path(node(nil, _, _, _, _ ), _, Path, Path) :- !.
build_path(node(EndState, _ , _ , _, _ ), Nodes, PartialPath, Path) :-
	del(Nodes, node(EndState, Action, Parent, _, _  ), Nodes1),
	build_path( node(Parent, _, _, _, _ ), Nodes1,
						[Action/EndState|PartialPath],Path).


del([X|R],X,R).
del([Y|R],X,[Y|R1]) :-
	X\=Y,
    del(R,X,R1).